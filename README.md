# myDebugWebviewApp #

This is a simple iOS application that contains 1 webview and 2 buttons to load either webpage content from the internet or local content from inside the App bundle.  The purpose is to show that the content of the webview can be debugged using the web inspector of safari on Mac.

### Debugging on mac  ###

* Install the myDebugWebviewApp.ipa on your iPad or iPhone
* Open Safari on mac and plug in the device using USB
* In Safari, click the Develop Menu -> *DeviceName* -> myDebugWebviewApp -> *PAGE_LOADED_IN_WEBVIEW*

### Requirements ###

* myDebugWebviewApp.ipa
* Mac with Safari installed