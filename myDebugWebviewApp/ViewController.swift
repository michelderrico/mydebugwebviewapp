//
//  ViewController.swift
//  myDebugWebviewApp
//
//  Created by Michel D'Errico on 2016-06-20.
//  Copyright © 2016 imshealth. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myWebView: UIWebView!
    @IBOutlet weak var loadWebPageButton: UIBarButtonItem!
    @IBOutlet weak var loadLocalButton: UIBarButtonItem!
    var URLString = "https://www.imshealth.com"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let htmlString = "<br><h2>Hi, weclome to my debug webview iOS app.  On a mac, please open Safari -> Develop -> <em>DeviceName</em> -> myDebugWebviewApp  to view the web inspector for this webview</h2>"
        myWebView.loadHTMLString(htmlString	, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func localLocalButtonTapped(sender: UIBarButtonItem) {
        // Do any additional setup after loading the view, typically from a nib.
        let localfilePath = NSBundle.mainBundle().URLForResource("index", withExtension: "html")
        let myRequest = NSURLRequest(URL: localfilePath!);
        myWebView.loadRequest(myRequest);
    }

    @IBAction func loadWebPageButtonTapped(sender: UIBarButtonItem) {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Load Web Page", message: "Enter a web address", preferredStyle: .Alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = self.URLString
        })
        
        //3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            print("Text field: \(textField.text)")
            self.URLString =  textField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            let url = NSURL (string: self.URLString);
            let requestObj = NSURLRequest(URL: url!);
            self.myWebView.loadRequest(requestObj);
        }))
        
        // 4. Present the alert.
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

